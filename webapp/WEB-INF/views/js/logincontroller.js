var app = angular.module('myapp');

//users
app.controller('login', ['$rootScope', '$scope', '$http', '$httpParamSerializerJQLike', 'UserFactory', '$location', function ($rootScope, $scope, $http, $httpParamSerializerJQLike, UserFactory, $location) {

    //$rootScope.loggedin=false;
    $scope.username = '';
    $scope.password = '';

    $scope.login = function () {
        var req = {
            method: 'POST',
            url: '/j_spring_security_check',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike({user: $scope.username, pass: $scope.password}),
        };

        $http(req).then(
            function (response) {
                var currentuser=UserFactory.current();
                currentuser.$promise.then(
                    function (data) {
                          if(data.principal!=null){
                              $rootScope.loggedin = true;
                              $rootScope.currentuser =  data.principal;
                              $location.path('/');
                          }
                        else{
                              $scope.err="не верный пароль или имя пользователя";
                          }
                    });
            },
            function (response) {
                window.alert('Проверьте сетевое подключение')
            });
        $location.path('/login');
    }

    $scope.logout = function () {
        $http.post('/logout', {}).finally(function() {
            $rootScope.loggedin = false;
            $location.path('/');
        });
    }
}]);
	
	