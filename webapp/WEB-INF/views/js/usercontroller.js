var app = angular.module('myapp');
//users
app.controller('users', ['$scope', 'UserFactory', '$location', function ($scope, UserFactory, $location) {
    $scope.users = UserFactory.query();

    $scope.createNewUser = function () {
        $location.path('/user');
    }

    $scope.editUser = function (id) {
        $location.path('/user/' + id);
    }

    $scope.deleteUser = function (id) {
        UserFactory.delete({id: id});
        $scope.users = UserFactory.query();
    }
}]);

//user
app.controller('user', ['$scope', '$routeParams', 'UserFactory', 'RolesFactory', '$location', function ($scope, $routeParams, UserFactory, RolesFactory, $location) {
    $scope.users = UserFactory.query();
    $scope.roles = RolesFactory.query();
    $scope.item = UserFactory.show({id: $routeParams.id});
    $scope.title = 'Изменить пользователя';

    $scope.updateUser = function () {
        UserFactory.update($scope.item);
        $location.path('/users');
    };

    $scope.cancel = function () {
        $location.path('/users');
    };
}]);

//newuser
app.controller('newuser', ['$scope', '$routeParams', 'UserFactory', 'RolesFactory', '$location', function ($scope, $routeParams, UserFactory, RolesFactory, $location) {
    $scope.users = UserFactory.query();
    $scope.roles = RolesFactory.query();
    $scope.title = 'Создать пользователя';

    $scope.updateUser = function () {
        UserFactory.create($scope.item);
        $location.path('/users');
    };

    $scope.cancel = function () {
        $location.path('/users');
    };
}]);
	