//services
var BASEURL = 'http://localhost:8080/rest';
var services = angular.module('myapp.services', ['ngResource']);

services.factory('RolesFactory', function ($resource) {
    return $resource(BASEURL + '/roles', {}, {
        query: {method: 'GET', isArray: true},
    })
});

services.factory('UserFactory', function ($resource) {
    return $resource(BASEURL + '/user/:id', {}, {
        query: {method: 'GET', isArray: true, url: BASEURL + '/users'},
        current: {method: 'GET', url: BASEURL + '/currentuser'},
        create: {method: 'POST', url: BASEURL + '/users'},
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});

services.factory('TaskFactory', function ($resource) {
    return $resource(BASEURL + '/task/:id', {}, {
        query: {method: 'GET', isArray: true, url: BASEURL + '/tasks'},
        create: {method: 'POST', url: BASEURL + '/tasks'},
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        move: {method: 'GET', url: BASEURL + '/task/moveto/:taskid/:listid'},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});

services.factory('TaskListFactory', function ($resource) {
    return $resource(BASEURL + '/tasklist/:id', {}, {
        query: {method: 'GET', isArray: true, url: BASEURL + '/tasklists'},
        create: {method: 'POST', url: BASEURL + '/tasklists'},
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});