var app = angular.module('myapp');

//routing
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {templateUrl: "html/main.html", controller: 'login'})
        .when('/users', {templateUrl: "html/users.html", controller: 'users'})
        .when('/user', {templateUrl: "html/user.html", controller: 'user'})
        .when('/user', {templateUrl: "html/user.html", controller: 'newuser'})
        .when('/user/:id', {templateUrl: "html/user.html", controller: 'user'})
        .when('/login', {templateUrl: "html/login.html", controller: 'login'})
        .when('/logout', {templateUrl: "html/login.html", controller: 'login'})
        .when('/tasks', {templateUrl: "html/tasks.html", controller: 'task'})
        .otherwise({redirectTo: '/'});
});