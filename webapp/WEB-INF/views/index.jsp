<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url value="/css/bootstrap.min.css" var="bootstrap"/>
<spring:url value="/css/index.css" var="indexcss"/>
<spring:url value="/css/style.css" var="stylecss"/>
<spring:url value="/css/signin.css" var="signincss"/>
<spring:url value="/css/bootstrap-slider.min.css" var="bootstrapmin"/>
<spring:url value="/css/bootstrap-datetimepicker.min.css" var="bootstrapdatepicker"/>

<!DOCTYPE html>
<html ng-app="myapp" ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Orgmanager2</title>
    <link href="${bootstrap}" rel="stylesheet">
    <link href="${indexcss}" rel="stylesheet">
    <link href="${stylecss}" rel="stylesheet">
    <link href="${signincss}" rel="stylesheet">
    <link href="${bootstrapmin}" rel="stylesheet">
    <link href="${bootstrapdatepicker}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <spring:url value="/js/lib/jquery-1.12.3.min.js" var="jquery"/>
    <script src="${jquery}"></script>

    <spring:url value="/js/lib/moment-with-locales.min.js" var="jquerylocjs"/>
    <script src="${jquerylocjs}"></script>

    <spring:url value="/js/lib/bootstrap-datetimepicker.min.js" var="bootstrapdatejs"/>
    <script src="${bootstrapdatejs}"></script>

    <spring:url value="/js/lib/bootstrap.min.js" var="bootstrapjs"/>
    <script src="${bootstrapjs}"></script>

    <spring:url value="/js/lib/bootstrap-slider.min.js" var="bootstrapsliderjs"/>
    <script src="${bootstrapsliderjs}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-resource.js"></script>

    <!-- Angular Material requires Angular.js Libraries -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>

    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0-rc2/angular-material.min.js"></script>

    <script src="./js/lib/angular-drag-and-drop-lists.js"></script>
    <script src="./js/appcontroller.js"></script>
    <script src="./js/taskcontroller.js"></script>
    <script src="./js/routing.js"></script>
    <script src="./js/service.js"></script>
    <script src="./js/usercontroller.js"></script>
    <script src="./js/logincontroller.js"></script>
</head>
<body>
<div ng-include="'layout/header.html'"></div>
<div ng-view></div>
<div ng-include="'layout/footer.html'"></div>
</body>
</html>
