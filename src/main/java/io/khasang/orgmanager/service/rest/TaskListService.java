package io.khasang.orgmanager.service.rest;


import io.khasang.orgmanager.dao.ITaskDao;
import io.khasang.orgmanager.dao.ITaskListDao;
import io.khasang.orgmanager.dao.IUserDao;
import io.khasang.orgmanager.model.Entities.Task;
import io.khasang.orgmanager.model.Entities.TaskList;
import io.khasang.orgmanager.model.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*")
public class TaskListService {
    @Autowired
    ITaskListDao taskListDao;

    @Autowired
    IUserDao userDao;

    private  User getCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        return userDao.getUserByName(name);
    }

    @RequestMapping( value = "/rest/tasklist/{id}", method = RequestMethod.GET)
    public TaskList getTask(@PathVariable("id") Integer id){
       return taskListDao.get(id);
    }

    @RequestMapping( value = "/rest/tasklists", method = RequestMethod.GET)
    public List<TaskList> getTaskLists() {
        List<TaskList> taskLists=taskListDao.getRelatedToUser(getCurrentUser());
        return taskLists;
    }

    @RequestMapping(value = "/rest/tasklists", method = RequestMethod.POST)
    public ResponseEntity<String> createlist(@RequestBody TaskList tasklist){
        tasklist.setOwner(getCurrentUser());
        taskListDao.save(tasklist);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/tasklist/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TaskList> updateTaskList(@PathVariable("id") int id, @RequestBody TaskList taskList){
        taskListDao.save(taskList);
        return new ResponseEntity<TaskList>(taskList, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/tasklist/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> updateTaskList(@PathVariable("id") int id)
    {
        taskListDao.delete(taskListDao.get(id));
        return new ResponseEntity(HttpStatus.OK);
    }
}
