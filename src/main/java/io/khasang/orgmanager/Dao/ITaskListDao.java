package io.khasang.orgmanager.dao;

import io.khasang.orgmanager.model.Entities.Task;
import io.khasang.orgmanager.model.Entities.TaskList;
import io.khasang.orgmanager.model.Entities.User;

import java.util.List;

public interface ITaskListDao extends GenericDao<TaskList> {
    List<TaskList> getRelatedToUser(User user);
}
