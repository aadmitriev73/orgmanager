package io.khasang.orgmanager.controller;

import io.khasang.orgmanager.dao.*;
import io.khasang.orgmanager.model.Entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;
import java.util.List;

@Controller
public class TestBdController {
    @Autowired
    IRoleDao roleDao;
    @Autowired
    IUserDao userDao;
    @Autowired
    ITaskListDao taskListDao;
    @Autowired
    ITaskDao taskDao;

    @RequestMapping("/createroles")
    public  String createroles(Model model){
        Role role=new Role();
        role.setName("USER");
        roleDao.save(role);
        Role adminrole=new Role();
        adminrole.setName("ADMIN");
        roleDao.save(adminrole);
        model.addAttribute("result","It seems to be ok");
        return "axilary";
    }
    @RequestMapping("/createuser")
    public  String createuser(Model model){
        User user=new User();
        user.setName("user");
        user.setPassword("password");
        Role role=roleDao.getRoleByName("USER");
        user.setRole(role);
        userDao.save(user);
        model.addAttribute("result","It seems to be ok");
        return "axilary";
    }

    @RequestMapping("/createadmin")
    public  String createadmin(Model model){
        User user=new User();
        user.setName("admin");
        user.setPassword("password");
        Role role=roleDao.getRoleByName("ADMIN");
        user.setRole(role);
        userDao.save(user);
        model.addAttribute("result","It seems to be ok");
        return "axilary";
    }

    @RequestMapping("/createlists")
    public  String createlists(Model model){
        User owner=userDao.getUserByName("admin");
        TaskList taskList=new TaskList();
        taskList.setOwner(owner);
        taskList.setName("backlog");
        taskListDao.save(taskList);

        taskList=new TaskList();
        taskList.setOwner(owner);
        taskList.setName("doing");
        taskListDao.save(taskList);

        taskList=new TaskList();
        taskList.setOwner(owner);
        taskList.setName("done");
        taskListDao.save(taskList);

        model.addAttribute("result","It seems to be ok");
        return "axilary";
    }

    @RequestMapping("/createtasks")
    public  String createtasks(Model model){
        User owner=userDao.getUserByName("admin");
        List<TaskList> taskListLists=taskListDao.getRelatedToUser(owner);
        for(TaskList taskList:taskListLists){
            Task task=new Task();
            task.setName("test task");
            task.setCreator(owner);
            task.setCreator(owner);
            task.setList(taskList);
            taskDao.save(task);
        }

        model.addAttribute("result","It seems to be ok");
        return "axilary";
    }
}
